<?php
/*
 * 2014-12-14
 * @author Programmer Thailand <contact@programmerthailand.com>
 * http://bootswatch.com/simplex/
 */
namespace kongoon\theme\simplex;

use yii\web\AssetBundle;
class SimplexAsset extends AssetBundle
{
    public $sourcePath='@vendor/kongoon/yii2-theme-simplex/assets';
    public $baseUrl = '@web';
    
    public $css=[
        'css/bootstrap.css',
        'css/style.css',
    ];
    
    public $js=[
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init() {
        parent::init();
    }
}
